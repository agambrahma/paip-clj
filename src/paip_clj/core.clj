(ns paip-clj.core
  (:require [paip-clj.util :as util]))

;; 2.3 Rule-based
(def *simple-grammar*
  "A trivial toy grammar"
  {:sentence  [[:noun-phrase :verb-phrase]]
   :noun-phrase  [[:article :noun]]
   :verb-phrase  [[:verb :noun-phrase]]
   :article  ["the" "a"]
   :noun  ["man" "ball" "woman" "table"]
   :verb  ["hit" "took" "saw" "liked"]})
   
(def ^:dynamic *grammar*
  "The grammar used by generate. Initiall, set to
  simple-grammar, but can be switched out later."

  *simple-grammar*)

; Don't need 'rule-lhs' and 'rule-rhs' from the original definition.

(defn rewrites
  "Get a list of the possible rewrites for a category"
  [category]
  (get *grammar* category))

(defn- generate-helper
  "Generate a random sentence or phrase"
  [phrase]
  (cond
    (vector? phrase) (mapcat generate-helper phrase)
    (seq (rewrites phrase)) (generate-helper (vec (util/one-of (rewrites phrase))))
    :else (list phrase)))

(defn generate
  "Wrapper that creates a nice string"
  [phrase]
  (clojure.string/join " " (generate-helper phrase)))

;;;; Examples:

;; user>  (generate :sentence)
;; "a ball took a table"
;; user>   (generate :sentence)
;; "the woman hit a man"

