(ns paip-clj.simple
  (:require [paip-clj.util :as util]))

; Initial set of sentence pieces
(defn verb
  []
  (util/one-of #{"hit" "took" "saw" "liked"}))

(defn noun
  []
  (util/one-of #{"man" "ball" "table" "woman"}))

(defn article
  []
  (util/one-of #{"a" "the"}))

(defn noun-phrase
  []
  (concat (article) (noun)))

(defn verb-phrase
  []
  (concat (verb) (noun-phrase)))

(defn sentence
  []
  (concat (noun-phrase) (verb-phrase)))

; Additional sentence pieces, some redefinitions.
(defn prep
  []
  (util/one-of #{"to" "in" "by" "with" "on"}))

(defn adj
  []
  (util/one-of #{"big" "little" "blue" "green" "adiabatic"}))

(defn pp
  []
  (concat (prep) (noun-phrase)))

(defn pp*
  []
  (if (util/coin-toss)
    '()
    (concat (pp) (pp*))))
    
(defn adj*
  []
  (if (util/coin-toss)
    '()
    (concat (adj) (adj*))))

(defn noun-phrase
  []
  (concat (article) (adj*) (noun) (pp*)))

