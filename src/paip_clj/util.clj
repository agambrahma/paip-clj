(ns paip-clj.util)


(defn one-of
  "Pick a random element out of a list, return as a seq"
  [s]
  (seq (list (rand-nth (seq s)))))

(defn coin-toss
  []
  (= 0 (rand-int 2)))
